package me.kodysimpson.persistentblocks;

import me.kodysimpson.persistentblocks.commands.SmeltCountCommand;
import me.kodysimpson.persistentblocks.listeners.CookListener;
import org.bukkit.plugin.java.JavaPlugin;

public final class PersistentBlocks extends JavaPlugin {

    private static PersistentBlocks plugin;

    @Override
    public void onEnable() {
        // Plugin startup logic

        getServer().getPluginManager().registerEvents(new CookListener(), this);

        getCommand("smeltcount").setExecutor(new SmeltCountCommand());

        plugin = this;

    }

    @Override
    public void onDisable() {
        // Plugin shutdown logic
    }

    public static PersistentBlocks getPlugin() {
        return plugin;
    }
}
