package me.kodysimpson.persistentblocks.commands;

import me.kodysimpson.persistentblocks.PersistentBlocks;
import org.bukkit.NamespacedKey;
import org.bukkit.block.Block;
import org.bukkit.block.TileState;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.persistence.PersistentDataContainer;
import org.bukkit.persistence.PersistentDataType;

public class SmeltCountCommand implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {

        if (sender instanceof Player){

            Player p = (Player) sender;

            Block b = p.getTargetBlockExact(5);

            if (b.getState() instanceof TileState){

                TileState tileState = (TileState) b.getState();

                PersistentDataContainer container = tileState.getPersistentDataContainer();

                if (container.has(new NamespacedKey(PersistentBlocks.getPlugin(), "smeltCount"), PersistentDataType.INTEGER)){

                    int currentAmount = container.get(new NamespacedKey(PersistentBlocks.getPlugin(), "smeltCount"), PersistentDataType.INTEGER);

                    p.sendMessage("The block has smelted " + currentAmount + " items");

                }else{

                    p.sendMessage("This block has not smelted any items.");

                }

            }else{
                p.sendMessage("not instanceof tilestate");
            }

        }

        return true;
    }
}
