package me.kodysimpson.persistentblocks.listeners;

import me.kodysimpson.persistentblocks.PersistentBlocks;
import org.bukkit.NamespacedKey;
import org.bukkit.block.Block;
import org.bukkit.block.BlockState;
import org.bukkit.block.TileState;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.FurnaceSmeltEvent;
import org.bukkit.persistence.PersistentDataContainer;
import org.bukkit.persistence.PersistentDataType;

public class CookListener implements Listener {

    @EventHandler
    public void smeltItem(FurnaceSmeltEvent e){

        Block block = e.getBlock();
        BlockState blockState = block.getState();

        if (blockState instanceof TileState){

            TileState tileState = (TileState) blockState;

            PersistentDataContainer container = tileState.getPersistentDataContainer();

            if (container.has(new NamespacedKey(PersistentBlocks.getPlugin(), "smeltCount"), PersistentDataType.INTEGER)){

                int currentAmount = container.get(new NamespacedKey(PersistentBlocks.getPlugin(), "smeltCount"), PersistentDataType.INTEGER);

                container.set(new NamespacedKey(PersistentBlocks.getPlugin(), "smeltCount"), PersistentDataType.INTEGER, currentAmount + 1);

            }else{

                container.set(new NamespacedKey(PersistentBlocks.getPlugin(), "smeltCount"), PersistentDataType.INTEGER, 1);

            }

            tileState.update();

        }

    }

}
